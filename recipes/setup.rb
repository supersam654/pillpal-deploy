# Web server packages: nodejs, git
Chef::Log.info('Installing PillPal dependencies...')
include_attribute 'setup'

include_recipe 'nodejs'
include_recipe 'git::default'
Chef::Log.info('Installed PillPal dependencies!')

Chef::Log.info('Making appropriate users')
user 'Add webserver user' do
  shell '/bin/false'
  username 'node'
end

user "Add deploy user" do
  home node[:deploy][:home]
  shell node[:deploy][:shell]
  username node[:deploy][:user]
end

group 'Add webserver group' do
  name 'node'
  members: ['node']
end
Chef::Log.info('Made appropriate users')
