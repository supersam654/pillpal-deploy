# Run at deploy time so we know the SSH key.
Chef::Log.info('Setting up Git key.')
directory "#{node[:deploy][:home]}/.ssh" do
  owner node[:deploy][:user]
  group node[:deploy][:group]
  mode "0700"
  action :create
  recursive true
end

file "#{node[:deploy][:home]}/.ssh/config" do
  owner node[:deploy][:user]
  group node[:deploy][:group]
  action :touch
  mode '0600'
end

execute "echo 'StrictHostKeyChecking no' > #{node[:deploy][:home]}/.ssh/config" do
  not_if "grep '^StrictHostKeyChecking no$' #{node[:deploy][:home]}/.ssh/config"
end

template "#{node[:deploy][:home]}/.ssh/id_rsa" do
  action :create
  mode '0600'
  owner node[:deploy][:user]
  group node[:deploy][:group]
  source 'ssh_key.erb'
  variables :ssh_key => node[:deploy][:scm][:ssh_key]
end
