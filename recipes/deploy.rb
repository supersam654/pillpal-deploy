# Basic deploy steps:
# 1) Pull from git
# 2) Setup environment
# 3) Restart node.js
Chef::Log.info('*** Deploying PillPal...')

node[:deploy].each do |application, deploy|
  if deploy[:application_type] != 'nodejs'
    Chef::Log.debug("Skipping deploy for application #{application} as it is not a node.js app")
    next
  end

  node[:deploy][:scm] = deploy[:scm]
  include_recipe 'deploy_git'

  Chef::Log.info('Pulling latest from Git.')
  git node[:deploy][:location] do
    repository          deploy[:scm][:repository]
    checkout_branch     deploy[:scm][:revision]
    depth               1
    enable_checkout     False
    user                node[:deploy][:user]
    group               node[:deploy][:group]
  end
end
