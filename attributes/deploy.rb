default['nodejs']['install_method'] = 'binary'
default['nodejs']['version'] = '4.4.5'
default['nodejs']['binary']['checksum']['linux_x64'] = '15d57c4a3696df8d5ef1bba452d38e5d27fc3c963760eeb218533c48381e89d5'
default['nodejs']['binary']['checksum']['linux_x86'] = '447b17542981c20f6792a20c31eb946c786e2cbd3bd1459f85c2cd0eb400e009'
