default[:deploy][:user] = 'deploy'
default[:deploy][:group] = 'deploy'
default[:deploy][:home] = '/home/deploy'
default[:deploy][:shell] = '/bin/bash'

default[:pillpal]['location'] = '/opt/pillpal'
